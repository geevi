#include <stdlib.h>
#include <string.h>
#include "im.h"

static struct im fa;

static struct im *get_fa()
{
	fa.map['a'] = "ش";
	fa.map['b'] = "ذ";
	fa.map['c'] = "ز";
	fa.map['d'] = "ی";
	fa.map['e'] = "ث";
	fa.map['f'] = "ب";
	fa.map['g'] = "ل";
	fa.map['h'] = "ا";
	fa.map['i'] = "ه";
	fa.map['j'] = "ت";
	fa.map['k'] = "ن";
	fa.map['l'] = "م";
	fa.map['m'] = "پ";
	fa.map['n'] = "د";
	fa.map['o'] = "خ";
	fa.map['p'] = "ح";
	fa.map['q'] = "ض";
	fa.map['r'] = "ق";
	fa.map['s'] = "س";
	fa.map['t'] = "ف";
	fa.map['u'] = "ع";
	fa.map['v'] = "ر";
	fa.map['w'] = "ص";
	fa.map['x'] = "ط";
	fa.map['y'] = "غ";
	fa.map['z'] = "ظ";

	fa.map['A'] = "ؤ";
	fa.map['B'] = "‌";
	fa.map['C'] = "ژ";
	fa.map['D'] = "ي";
	fa.map['E'] = "ٍ";
	fa.map['F'] = "إ";
	fa.map['G'] = "أ";
	fa.map['H'] = "آ";
	fa.map['I'] = "ّ";
	fa.map['J'] = "ة";
	fa.map['K'] = "»";
	fa.map['L'] = "«";
	fa.map['M'] = "ء";
	fa.map['N'] = "ٔ";
	fa.map['O'] = "]";
	fa.map['P'] = "[";
	fa.map['Q'] = "ْ";
	fa.map['R'] = "ً";
	fa.map['S'] = "ئ";
	fa.map['T'] = "ُ";
	fa.map['U'] = "َ";
	fa.map['V'] = "ٰ";
	fa.map['W'] = "ٌ";
	fa.map['X'] = "ٓ";
	fa.map['Y'] = "ِ";
	fa.map['Z'] = "ك";

	fa.map[';'] = "ک";
	fa.map['\''] ="گ";
	fa.map['['] = "ج";
	fa.map[']'] = "چ";
	fa.map['\\'] = "\\";
	fa.map['/'] = "/";
	fa.map[','] = "و";
	fa.map['.'] = ".";

	fa.map[':'] = ":";
	fa.map['"'] = "؛";
	fa.map['{'] = "}";
	fa.map['}'] = "{";
	fa.map['|'] = "|";
	fa.map['?'] = "؟";
	fa.map['<'] = ">";
	fa.map['>'] = "<";

	fa.map['`'] = "‍";
	fa.map['1'] = "۱";
	fa.map['2'] = "۲";
	fa.map['3'] = "۳";
	fa.map['4'] = "۴";
	fa.map['5'] = "۵";
	fa.map['6'] = "۶";
	fa.map['7'] = "۷";
	fa.map['8'] = "۸";
	fa.map['9'] = "۹";
	fa.map['0'] = "۰";
	fa.map['-'] = "-";
	fa.map['='] = "=";
	fa.map['~'] = "÷";
	fa.map['!'] = "!";
	fa.map['@'] = "٬";
	fa.map['#'] = "٫";
	fa.map['$'] = "﷼";
	fa.map['%'] = "٪";
	fa.map['^'] = "×";
	fa.map['&'] = "،";
	fa.map['*'] = "*";
	fa.map['('] = ")";
	fa.map[')'] = "(";
	fa.map['_'] = "ـ";
	fa.map['+'] = "+";
	return &fa;
}

struct im *im_get(char *name)
{
	if (!strcmp(name, "fa"))
		return get_fa();
	return NULL;
}

char *im_char(struct im *im, int c)
{
	if (c < 256 && im->map[c])
		return im->map[c];
	return NULL;
}
