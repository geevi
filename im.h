#ifndef _IM_H
#define _IM_H

struct im {
	char *map[256];
};

struct im *im_get(char *name);
char *im_char(struct im *im, int c);

#endif
