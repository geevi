#ifndef _GEEVI_H
#define _GEEVI_H

#define GEEVI_VERSION		"0.1"
#define MAX_COMMAND_LENGTH	64

enum editing_mode {
	EM_NORMAL,
	EM_INSERT,
	EM_EX
};

enum movekind {
	MM_CHAR,
	MM_LINE,
	MM_EOL,
	MM_WORDEND,
	MM_WORDSTART,
	MM_FIRST,
	MM_FIRSTNB,
	MM_GOTOLINE,
	MM_WHOLELINE,
	MM_MARK,
	MM_TOP,
	MM_CENTER,
	MM_BOTTOM,
	MM_SEARCH,
	MM_CFIND
};

struct movement {
	enum movekind kind;
	int count;
	/* movement argument */
	union {
		int m;
		int c;
		char *s;
	} arg;
	/* in normal mode the cursor cannot be at eol */
	unsigned eolfix:1;
	/* move one more char */
	unsigned onemore:1;
	/* movement not completed yet */
	unsigned pending:1;
};

struct vi {
	enum editing_mode mode;
	/* the text view that shows text */
	GtkWidget *view;
	/* the command line entry */
	GtkWidget *entry;
	char filename[FILENAME_MAX];
	int (*ctx)(struct vi *vi, GdkEventKey *event);
	/* alternate input method */
	struct im *alt_im;
	struct im *im;
	/* current movement */
	struct movement mm;
	/* search keyword */
	char word[MAX_COMMAND_LENGTH];
};

void moveiter(GtkTextView *view, struct movement *mm,
	      GtkTextIter *iter, GtkTextIter *start);
void execcmd(struct vi *vi, char *cmd);
void edit(struct vi *vi, char *filename);
int rootctx(struct vi *vi, GdkEventKey *event);
int ex_ctx(struct vi *vi, GdkEventKey *event);

void cursor_init(GtkTextView *view);
void cursor_place(GtkTextView *view, GtkTextIter *iter);
void cursor_get(GtkTextView *view, GtkTextIter *iter);
void cursor_redraw(GtkTextView *view);

#endif
