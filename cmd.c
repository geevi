#include <string.h>
#include <ctype.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include "geevi.h"
#include "im.h"

static void mkmm(struct movement *mm, int count, enum movekind kind)
{
	mm->count = count;
	mm->kind = kind;
	mm->eolfix = 1;
}

static void mkmm_noeolfix(struct movement *mm, int count, enum movekind kind)
{
	mkmm(mm, count, kind);
	mm->eolfix = 0;
}

static void command_line_mode(struct vi *vi, char *init);

static int read_movement_key(struct vi *vi, GdkEventKey *event)
{
	struct movement *mm = &vi->mm;
	if (mm->pending) {
		mm->pending = 0;
		switch (mm->kind) {
		case MM_MARK:
			mm->arg.m = event->keyval;
			return 0;
		case MM_SEARCH:
			mm->arg.s = vi->word;
			return 0;
		case MM_CFIND:
			mm->arg.c = event->keyval;
			return 0;
		default:
			return 1;
		}
	}
	memset(&vi->mm, 0, sizeof(vi->mm));
	switch(event->keyval) {
	case GDK_j:
		mkmm(mm, 1, MM_LINE);
		break;
	case GDK_k:
		mkmm(mm, -1, MM_LINE);
		break;
	case GDK_l:
		mkmm(mm, 1, MM_CHAR);
		break;
	case GDK_h:
		mkmm(mm, -1, MM_CHAR);
		break;
	case GDK_dollar:
		mkmm(mm, 1, MM_EOL);
		break;
	case GDK_e:
		mkmm(mm, 1, MM_WORDEND);
		break;
	case GDK_b:
		mkmm(mm, -1, MM_WORDSTART);
		break;
	case GDK_w:
		mkmm(mm, 1, MM_WORDSTART);
		break;
	case GDK_0:
		mkmm(mm, 1, MM_FIRST);
		break;
	case GDK_asciicircum:
		mkmm(mm, 1, MM_FIRSTNB);
		break;
	case GDK_G:
		mkmm(mm, 1, MM_GOTOLINE);
		break;
	case GDK_d:
		mkmm_noeolfix(mm, 1, MM_WHOLELINE);
		mm->onemore = 1;
		break;
	case GDK_c:
		mkmm_noeolfix(mm, 1, MM_WHOLELINE);
		break;
	case GDK_quoteleft:
		mkmm(mm, 1, MM_MARK);
		mm->pending = 1;
		break;
	case GDK_H:
		mkmm(mm, 1, MM_TOP);
		break;
	case GDK_M:
		mkmm(mm, 1, MM_CENTER);
		break;
	case GDK_L:
		mkmm(mm, 1, MM_BOTTOM);
		break;
	case GDK_slash:
		mkmm(mm, 1, MM_SEARCH);
		mm->pending = 1;
		command_line_mode(vi, "/");
		break;
	case GDK_question:
		mkmm(mm, -1, MM_SEARCH);
		mm->pending = 1;
		command_line_mode(vi, "?");
		break;
	case GDK_n:
		mkmm(mm, 1, MM_SEARCH);
		mm->arg.s = vi->word;
		break;
	case GDK_N:
		mkmm(mm, -1, MM_SEARCH);
		mm->arg.s = vi->word;
		break;
	case GDK_f:
		mkmm(mm, 1, MM_CFIND);
		mm->pending = 1;
		break;
	case GDK_F:
		mkmm(mm, -1, MM_CFIND);
		mm->pending = 1;
		break;
	default:
		return 1;
	}
	return 0;
}

static void set_mm_onemore(struct movement *mm)
{
	if (mm->kind == MM_WORDEND || mm->kind == MM_EOL)
		mm->onemore = 1;
}

static GtkTextBuffer *vi_buffer(struct vi *vi)
{
	return gtk_text_view_get_buffer(GTK_TEXT_VIEW(vi->view));
}

static void insert_mode(struct vi *vi);

static void perform_move_command(
		struct vi *vi, struct movement *mm,
		void (*perform)(struct vi *vi, GtkTextIter *iter,
				GtkTextIter *start))
{
	GtkTextIter start, iter;
	cursor_get(GTK_TEXT_VIEW(vi->view), &iter);
	start = iter;
	moveiter(GTK_TEXT_VIEW(vi->view), mm, &iter, &start);
	perform(vi, &iter, &start);
	cursor_place(GTK_TEXT_VIEW(vi->view), &iter);
}

static void dodel(struct vi *vi, GtkTextIter *iter, GtkTextIter *start)
{
	gtk_text_buffer_delete(vi_buffer(vi), start, iter);
	cursor_place(GTK_TEXT_VIEW(vi->view), iter);
}

static void domove(struct vi *vi, GtkTextIter *iter, GtkTextIter *start)
{
	cursor_place(GTK_TEXT_VIEW(vi->view), iter);
}

static void dochange(struct vi *vi, GtkTextIter *iter, GtkTextIter *start)
{
	gtk_text_buffer_delete(vi_buffer(vi), start, iter);
	insert_mode(vi);
}

static int delctx(struct vi *vi, GdkEventKey *event)
{
	if (!read_movement_key(vi, event)) {
		if (vi->mm.pending)
			return 0;
		set_mm_onemore(&vi->mm);
		perform_move_command(vi, &vi->mm, dodel);
	}
	return 1;
}

static int changectx(struct vi *vi, GdkEventKey *event)
{
	if (!read_movement_key(vi, event)) {
		if (vi->mm.pending)
			return 0;
		set_mm_onemore(&vi->mm);
		perform_move_command(vi, &vi->mm, dochange);
	}
	return 1;
}

static int scrollctx(struct vi *vi, GdkEventKey *event)
{
	GtkTextIter iter;
	double align;
	cursor_get(GTK_TEXT_VIEW(vi->view), &iter);
	switch(event->keyval) {
	case GDK_Return:
		align = 0.0;
		break;
	case GDK_period:
		align = 0.5;
		break;
	case GDK_minus:
		align = 1.0;
		break;
	default:
		return 1;
	}
	gtk_text_iter_set_line_offset(&iter, 0);
	gtk_text_view_scroll_to_iter(GTK_TEXT_VIEW(vi->view), &iter,
				     0.0, TRUE, 0, align);
	return 1;
}

static int replacectx(struct vi *vi, GdkEventKey *event)
{
	GtkTextIter start, end;
	GtkTextBuffer *buffer = vi_buffer(vi);
	char *imc;
	int key = event->keyval;
	int mod = event->state & gtk_accelerator_get_default_mod_mask();
	if (mod == GDK_CONTROL_MASK)
		return 1;
	cursor_get(GTK_TEXT_VIEW(vi->view), &start);
	end = start;
	gtk_text_iter_forward_char(&end);
	gtk_text_buffer_delete(buffer, &start, &end);
	if (vi->im && (imc = im_char(vi->im, key))) {
		gtk_text_buffer_insert(buffer, &start, imc, strlen(imc));
	} else {
		char c = key;
		gtk_text_buffer_insert(buffer, &start, &c, 1);
	}
	cursor_redraw(GTK_TEXT_VIEW(vi->view));
	return 1;
}

static int markctx(struct vi *vi, GdkEventKey *event)
{
	GtkTextIter iter;
	GtkTextBuffer *buffer = vi_buffer(vi);
	cursor_get(GTK_TEXT_VIEW(vi->view), &iter);
	if (isprint(event->keyval)) {
		char name[2];
		name[0] = event->keyval;
		name[1] = '\0';
		gtk_text_buffer_create_mark(buffer, name, &iter, 1);
	}
	return 1;
}

static void move(struct vi *vi, struct movement *mm)
{
	perform_move_command(vi, mm, domove);
}

static void normal_mode(struct vi *vi)
{
	struct movement mm;
	if (vi->mode == EM_INSERT) {
		mkmm(&mm, -1, MM_CHAR);
		move(vi, &mm);
	}
	vi->mode = EM_NORMAL;
	gtk_widget_grab_focus(vi->view);
}

static void insert_mode(struct vi *vi)
{
	vi->mode = EM_INSERT;
	gtk_widget_grab_focus(vi->view);
}

static void ex_mode(struct vi *vi)
{
	vi->mode = EM_EX;
	gtk_widget_grab_focus(vi->entry);
}

static void command_line_mode(struct vi *vi, char *init)
{
	ex_mode(vi);
	gtk_entry_set_text(GTK_ENTRY(vi->entry), init);
	gtk_editable_set_position(GTK_EDITABLE(vi->entry), -1);
}

static void move_page(GtkTextView *view, int count)
{
	GdkRectangle rect;
	GtkTextIter iter;
	GtkTextBuffer *buffer;
	int y;
	int yalign = count > 0 ? 0.0 : 1.0;
	buffer = gtk_text_view_get_buffer(view);
	gtk_text_view_get_visible_rect(view, &rect);
	y = rect.y + (count > 0 ? count : count + 1) * rect.height;
	gtk_text_view_get_iter_at_location(view, &iter, 0, y);
	cursor_place(view, &iter);
	gtk_text_view_scroll_to_iter(GTK_TEXT_VIEW(view), &iter,
				     0.0, TRUE, 0, yalign);
}

static void openline(struct vi *vi, int n)
{
	GtkTextIter iter;
	GtkTextBuffer *buffer = vi_buffer(vi);
	cursor_get(GTK_TEXT_VIEW(vi->view), &iter);
	if (n > 0) {
		gtk_text_iter_forward_to_line_end(&iter);
	} else {
		gtk_text_iter_set_line_offset(&iter, 0);
		gtk_text_iter_backward_char(&iter);
	}
	gtk_text_buffer_insert(buffer, &iter, "\n", 1);
	cursor_place(GTK_TEXT_VIEW(vi->view), &iter);
	insert_mode(vi);
}

static void rmchar(struct vi *vi, int n)
{
	GtkTextIter start, end;
	GtkTextBuffer *buffer = vi_buffer(vi);
	cursor_get(GTK_TEXT_VIEW(vi->view), &start);
	end = start;
	if (n > 0)
		gtk_text_iter_forward_char(&end);
	else
		gtk_text_iter_backward_char(&start);
	gtk_text_buffer_delete(buffer, &start, &end);
	cursor_redraw(GTK_TEXT_VIEW(vi->view));
}

static void join(struct vi *vi, int n)
{
	GtkTextIter start, end;
	GtkTextBuffer *buffer = vi_buffer(vi);
	cursor_get(GTK_TEXT_VIEW(vi->view), &start);
	if (!gtk_text_iter_ends_line(&start))
		gtk_text_iter_forward_to_line_end(&start);
	end = start;
	gtk_text_iter_forward_char(&end);
	gtk_text_buffer_delete(buffer, &start, &end);
	cursor_place(GTK_TEXT_VIEW(vi->view), &end);
}

static void del_till_eol(struct vi *vi, int change)
{
	GtkTextIter start, end;
	GtkTextBuffer *buffer = vi_buffer(vi);
	cursor_get(GTK_TEXT_VIEW(vi->view), &start);
	end = start;
	if (!gtk_text_iter_ends_line(&end))
		gtk_text_iter_forward_to_line_end(&end);
	gtk_text_buffer_delete(buffer, &start, &end);
	if (change) {
		insert_mode(vi);
	} else if (!gtk_text_iter_starts_line(&start)) {
		gtk_text_iter_backward_char(&start);
		cursor_place(GTK_TEXT_VIEW(vi->view), &start);
	}
}

static int im_insert(struct vi *vi, int c)
{
	char *s;
	GtkTextBuffer *buffer = vi_buffer(vi);
	s = im_char(vi->im, c);
	if (s) {
		gtk_text_buffer_insert_at_cursor(buffer, s, strlen(s));
		return 0;
	}
	return 1;
}

int ex_ctx(struct vi *vi, GdkEventKey *event)
{
	int key = event->keyval;
	int mod = event->state & gtk_accelerator_get_default_mod_mask();
	if (key == GDK_Escape) {
		normal_mode(vi);
		return 1;
	}
	/* execute command */
	if (key == GDK_Return) {
		char cmd[MAX_COMMAND_LENGTH];
		strncpy(cmd, gtk_entry_get_text(GTK_ENTRY(vi->entry)),
			sizeof(cmd));
		cmd[sizeof(cmd) - 1] = '\0';
		execcmd(vi, cmd);
		normal_mode(vi);
		if (vi->mm.pending)
			vi->ctx(vi, event);
		return 1;
	}
	/* insert normal keys */
	if (mod != GDK_CONTROL_MASK)
		return FALSE;
	switch (key) {
	case GDK_bracketleft:
	case GDK_c:
		normal_mode(vi);
		return 1;
	default:
		return 0;
	}
}

static int rootctx_insert(struct vi *vi, GdkEventKey *event)
{
	int key = event->keyval;
	int mod = event->state & gtk_accelerator_get_default_mod_mask();
	if (key == GDK_Escape) {
		normal_mode(vi);
		return 1;
	}
	if (mod != GDK_CONTROL_MASK)
		return vi->im && !im_insert(vi, key);
	switch (key) {
	case GDK_bracketleft:
	case GDK_c:
		normal_mode(vi);
		return 1;
	case GDK_6:
		if (vi->im)
			vi->im = NULL;
		else
			vi->im = vi->alt_im;
		break;
	default:
		return 0;
	}
	return 1;
}

static int real_key(int c)
{
	return c < 256 || c == GDK_Return;
}

int rootctx(struct vi *vi, GdkEventKey *event)
{
	int mod = event->state & gtk_accelerator_get_default_mod_mask();
	struct movement mm;
	if (!real_key(event->keyval))
		return 0;
	if (vi->mode == EM_INSERT)
		return rootctx_insert(vi, event);
	/* handle normal mode binding */
	if (vi->mm.pending) {
		if (!read_movement_key(vi, event))
			perform_move_command(vi, &vi->mm, domove);
		return 1;
	}
	if (mod == GDK_CONTROL_MASK) {
		switch (event->keyval) {
		case GDK_f:
			move_page(GTK_TEXT_VIEW(vi->view), 1);
			break;
		case GDK_b:
			move_page(GTK_TEXT_VIEW(vi->view), -1);
			break;
		default:
			break;
		}
		return 1;
	}
	switch (event->keyval) {
	case GDK_i:
		insert_mode(vi);
		break;
	case GDK_a:
		mkmm_noeolfix(&mm, 1, MM_CHAR);
		move(vi, &mm);
		insert_mode(vi);
		break;
	case GDK_A:
		mkmm_noeolfix(&mm, 1, MM_EOL);
		move(vi, &mm);
		insert_mode(vi);
		break;
	case GDK_I:
		mkmm_noeolfix(&mm, 1, MM_FIRSTNB);
		move(vi, &mm);
		insert_mode(vi);
		break;
	case GDK_colon:
		command_line_mode(vi, ":");
		break;
	case GDK_d:
		vi->ctx = delctx;
		break;
	case GDK_c:
		vi->ctx = changectx;
		break;
	case GDK_z:
		vi->ctx = scrollctx;
		break;
	case GDK_o:
		openline(vi, 1);
		break;
	case GDK_O:
		openline(vi, -1);
		break;
	case GDK_x:
		rmchar(vi, 1);
		break;
	case GDK_X:
		rmchar(vi, -1);
		break;
	case GDK_J:
		join(vi, 1);
		break;
	case GDK_r:
		vi->ctx = replacectx;
		break;
	case GDK_m:
		vi->ctx = markctx;
		break;
	case GDK_D:
		del_till_eol(vi, 0);
		break;
	case GDK_C:
		del_till_eol(vi, 1);
		break;
	default:
		if (!read_movement_key(vi, event)) {
			if (vi->mm.pending)
				break;
			perform_move_command(vi, &vi->mm, domove);
		}
	}
	return 1;
}
