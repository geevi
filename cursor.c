#include <gtk/gtk.h>
#include "geevi.h"

#define CURSOR_MARK		"geevi_cursor"
#define CURSOR_TAG		"geevi_tag"

void cursor_init(GtkTextView *view)
{
	GtkTextBuffer *buffer = gtk_text_view_get_buffer(view);
	GtkTextIter iter;
	gtk_text_buffer_get_start_iter(buffer, &iter);
	gtk_text_buffer_create_mark(buffer, CURSOR_MARK, &iter, FALSE);
	gtk_text_buffer_create_tag(buffer, CURSOR_TAG,
				   "background", "#000000",
				   "foreground", "#FFFFFF", NULL);
	cursor_place(view, &iter);
}

void cursor_redraw(GtkTextView *view)
{
	GtkTextIter iter;
	cursor_get(view, &iter);
	cursor_place(view, &iter);
}

void cursor_place(GtkTextView *view, GtkTextIter *iter)
{
	GtkTextIter old_start, old_end;
	GtkTextIter end = *iter;
	GtkTextBuffer *buffer = gtk_text_view_get_buffer(view);
	GtkTextMark *mark = gtk_text_buffer_get_mark(buffer, CURSOR_MARK);
	cursor_get(view, &old_start);
	old_end = old_start;
	gtk_text_iter_forward_char(&old_end);
	gtk_text_buffer_remove_tag_by_name(buffer, CURSOR_TAG,
					   &old_start, &old_end);
	if (gtk_text_iter_ends_line(iter)) {
		/* cursor is hidden... do something! */
	}
	gtk_text_buffer_move_mark(buffer, mark, iter);

	gtk_text_iter_forward_char(&end);
	gtk_text_buffer_apply_tag_by_name(buffer, CURSOR_TAG, iter, &end);
	gtk_text_buffer_place_cursor(buffer, iter);
	gtk_text_view_scroll_mark_onscreen(view, mark);
}

void cursor_get(GtkTextView *view, GtkTextIter *iter)
{
	GtkTextMark *mark;
	GtkTextBuffer *buffer = gtk_text_view_get_buffer(view);
	mark = gtk_text_buffer_get_mark(buffer, CURSOR_MARK);
	gtk_text_buffer_get_iter_at_mark(buffer, iter, mark);
}
