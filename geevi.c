#include <ctype.h>
#include <getopt.h>
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include "geevi.h"
#include "im.h"

static void on_window_destroy(GtkWidget *widget, gpointer data)
{
	gtk_main_quit();
}

static int is_real_key(int keyval)
{
	return isprint(keyval) || keyval == GDK_Return;
}

static gboolean on_key_press(GtkWidget *widget, GdkEventKey *event,
			     gpointer data)
{
	struct vi *vi = data;
	if (vi->mode == EM_EX) {
		if (ex_ctx(vi, event)) {
			vi->ctx = rootctx;
			return 1;
		}
		return 0;
	}
	if (vi->ctx == rootctx)
		return vi->ctx(vi, event);
	if (is_real_key(event->keyval) && vi->ctx(vi, event))
		vi->ctx = rootctx;
	return TRUE;
}

static void set_widget_font(GtkWidget *widget, char *font)
{
	PangoFontDescription *desc;
	desc = pango_font_description_from_string(font);
	gtk_widget_modify_font(widget, desc);
	pango_font_description_free(desc);
}

static void draw_editor(struct vi *vi, char *font)
{
	GtkWidget *window;
	GtkWidget *vbox;
	GtkTextBuffer *buffer;
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(window), "Simple GTK VI");
	g_signal_connect(G_OBJECT(window), "destroy",
			 G_CALLBACK(on_window_destroy), NULL);

	vbox = gtk_vbox_new(FALSE, 2);
	gtk_container_add(GTK_CONTAINER(window), vbox);

	/* create the textview */
	vi->view = gtk_text_view_new();
	gtk_box_pack_start(GTK_BOX(vbox), vi->view, 1, 1, 0);
	gtk_signal_connect(GTK_OBJECT(vi->view), "key_press_event",
			   GTK_SIGNAL_FUNC(on_key_press), vi);
	set_widget_font(vi->view, font);
	gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(vi->view),
				    GTK_WRAP_CHAR);
	gtk_text_view_set_cursor_visible(GTK_TEXT_VIEW(vi->view), FALSE);

	/* write something */
	buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(vi->view));
	gtk_text_buffer_set_text(buffer, "some text...\n", -1);

	/* create the minibuffer */
	vi->entry = gtk_entry_new();
	gtk_box_pack_start(GTK_BOX(vbox), vi->entry, 0, 0, 0);
	gtk_signal_connect(GTK_OBJECT(vi->entry), "key_press_event",
			   GTK_SIGNAL_FUNC(on_key_press), vi);
	set_widget_font(vi->entry, "DejaVuSansMono 9");
	gtk_widget_set_size_request(window, 800, 600);
	gtk_window_set_default_size(GTK_WINDOW(window), 1024, 768);

	gtk_widget_show_all(window);
}

static char *usage =
	"Usage: %s [ARGS]\n\n"
	"Arguments:\n"
	"  -f font	set text font\n"
	"  -h		show this\n"
	"  -v		show version\n";

static void print_help(int argc, char **argv)
{
	printf(usage, argv[0]);
}

static char *version =
	"GEEVI %s\n"
	"Copyright (C) 2009 Ali Gholami Rudi\n\n"
	"You can redistribute and/or modify geevi under the terms\n"
	"of GNU General Public License.\n";

static void print_version(int argc, char **argv)
{
	printf(version, GEEVI_VERSION);
}

int main(int argc, char *argv[])
{
	struct vi vi;
	int c;
	char *font = "DejaVuSansMono 10";
	memset(&vi, 0, sizeof(vi));
	vi.ctx = rootctx;
	while ((c = getopt(argc, argv, "vhf:")) != -1) {
		switch (c) {
		case 'v':
			print_version(argc, argv);
			return 0;
		case 'h':
			print_help(argc, argv);
			return 0;
		case 'f':
			font = optarg;
			break;
		case '?':
		default:
			fprintf(stderr, "Bad command line option\n");
			return 1;
		}
	}
	vi.alt_im = im_get("fa");
	gtk_init(&argc, &argv);
	draw_editor(&vi, font);
	cursor_init(GTK_TEXT_VIEW(vi.view));
	if (optind < argc) {
		edit(&vi, argv[optind]);
	}
	gtk_main();
	return 0;
}
