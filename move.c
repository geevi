#include <ctype.h>
#include <gtk/gtk.h>
#include "geevi.h"

static int line_length(GtkTextIter *orig)
{
	GtkTextIter iter = *orig;
	while (!gtk_text_iter_ends_line(&iter))
		gtk_text_iter_forward_char(&iter);
	return gtk_text_iter_get_line_offset(&iter);
}

static void jump(GtkTextIter *iter, int c)
{
	char name[2];
	GtkTextMark *mark;
	GtkTextBuffer *buffer = gtk_text_iter_get_buffer(iter);
	name[0] = c;
	name[1] = '\0';
	mark = gtk_text_buffer_get_mark(buffer, name);
	if (mark)
		gtk_text_buffer_get_iter_at_mark(buffer, iter, mark);
}

/* where: 0, 1, 2 for top, center, buttom respectively */
static void recenter(GtkTextView *view, int where, GtkTextIter *iter)
{
	GdkRectangle rect;
	int y;
	gtk_text_view_get_visible_rect(view, &rect);
	switch (where) {
	case 1:
		y = rect.y + rect.height / 2;
		break;
	case 2:
		y = rect.y + rect.height;
		break;
	default:
		y = rect.y;
	}
	gtk_text_view_get_iter_at_location(view, iter, 0, y);
}

static void search(struct movement *mm, GtkTextIter *iter)
{
	GtkTextIter start, end;
	int found = 0;
	start = end = *iter;
	if (mm->count > 0) {
		gtk_text_iter_forward_char(&start);
		found = gtk_text_iter_forward_search(&start, mm->arg.s, 0,
						     &start, &end, NULL);
	} else {
		gtk_text_iter_backward_char(&start);
		found = gtk_text_iter_backward_search(&end, mm->arg.s, 0,
						      &start, &end, NULL);
	}
	if (found)
		*iter = start;
}

static gboolean ischar(gunichar c, void *data)
{
	struct movement *mm = data;
	return c == mm->arg.c;
}

static void cfind(struct movement *mm, GtkTextIter *iter)
{
	GtkTextIter cur = *iter;
	GtkTextIter bol = *iter;
	GtkTextIter eol = *iter;
	int found = 0;
	gtk_text_iter_forward_to_line_end(&eol);
	gtk_text_iter_set_line_offset(&bol, 0);
	if (mm->count > 0) {
		gtk_text_iter_forward_char(&cur);
		found = gtk_text_iter_forward_find_char(&cur, ischar,
							mm, NULL);
	} else {
		gtk_text_iter_backward_char(&cur);
		found = gtk_text_iter_backward_find_char(&cur, ischar,
							 mm, NULL);
	}
	if (found && gtk_text_iter_compare(&bol, &cur) <= 0 &&
	    gtk_text_iter_compare(&eol, &cur) >= 0)
		*iter = cur;
}

void moveiter(GtkTextView *view, struct movement *mm,
	      GtkTextIter *iter, GtkTextIter *start)
{
	GtkTextIter orig;
	int length;
	int offset = -1;
	switch (mm->kind) {
	case MM_LINE:
		offset = gtk_text_iter_get_line_offset(iter);
		gtk_text_iter_forward_lines(iter, mm->count);
		break;
	case MM_CHAR:
		orig = *iter;
		gtk_text_view_move_visually(view, iter, mm->count);
		if (gtk_text_iter_get_line(iter) !=
		    gtk_text_iter_get_line(&orig))
			*iter = orig;
		break;
	case MM_EOL:
		gtk_text_iter_forward_to_line_end(iter);
		break;
	case MM_WORDEND:
		if (gtk_text_iter_forward_char(iter) &&
		    gtk_text_iter_forward_word_end(iter))
			gtk_text_iter_backward_char(iter);
		break;
	case MM_WORDSTART:
		if (mm->count > 0) {
			if (!isspace(gtk_text_iter_get_char(iter)))
				gtk_text_iter_forward_word_end(iter);
			if (gtk_text_iter_forward_word_end(iter))
				gtk_text_iter_backward_word_start(iter);
		}
		if (mm->count < 0)
			gtk_text_iter_backward_word_start(iter);
		break;
	case MM_FIRST:
		gtk_text_iter_set_line_offset(iter, 0);
		break;
	case MM_FIRSTNB:
		gtk_text_iter_set_line_offset(iter, 0);
		while (isspace(gtk_text_iter_get_char(iter)) &&
		       !gtk_text_iter_ends_line(iter) &&
		       gtk_text_iter_forward_char(iter))
			;
		break;
	case MM_GOTOLINE:
		gtk_text_iter_forward_to_end(iter);
		gtk_text_iter_set_line_offset(iter, 0);
		break;
	case MM_WHOLELINE:
		gtk_text_iter_set_line_offset(start, 0);
		gtk_text_iter_forward_to_line_end(iter);
		break;
	case MM_MARK:
		jump(iter, mm->arg.m);
		break;
	case MM_TOP:
		recenter(view, 0, iter);
		break;
	case MM_CENTER:
		recenter(view, 1, iter);
		break;
	case MM_BOTTOM:
		recenter(view, 2, iter);
		break;
	case MM_SEARCH:
		search(mm, iter);
		break;
	case MM_CFIND:
		cfind(mm, iter);
		break;
	}
	if (offset == -1)
		offset = gtk_text_iter_get_line_offset(iter);
	length = line_length(iter);
	if (offset > length)
		offset = length;
	if (mm->eolfix && offset == length)
		offset--;
	if (offset < 0 || !length)
		offset = 0;
	gtk_text_iter_set_line_offset(iter, offset);
	if (mm->onemore)
		gtk_text_iter_forward_char(iter);
}
