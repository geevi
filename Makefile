CC = cc
CFLAGS = -std=c89 -pedantic -Wall -O2 `pkg-config --cflags gtk+-2.0`
LDFLAGS = `pkg-config --libs gtk+-2.0`

all: geevi
.c.o:
	$(CC) -c $(CFLAGS) $<
geevi: geevi.o move.o ex.o cmd.o im.o cursor.o
	$(CC) $(LDFLAGS) -o $@ $^
clean:
	rm -f *.o geevi
ctags:
	ctags *.[hc]
