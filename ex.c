#include <stdlib.h>
#include <string.h>
#include <gtk/gtk.h>
#include "geevi.h"

void edit(struct vi *vi, char *name)
{
	char *text;
	size_t length;
	GtkTextIter iter;
	GtkTextBuffer *buffer;
	if (!*name)
		return;
	strcpy(vi->filename, name);
	buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(vi->view));
	if (!g_file_get_contents(name, &text, &length, NULL)) {
		fprintf(stderr, "could not open file\n");
		gtk_text_buffer_set_text(buffer, "", -1);
		return;
	}
	gtk_text_buffer_set_text(buffer, text, -1);
	gtk_text_buffer_get_start_iter(buffer, &iter);
	cursor_place(GTK_TEXT_VIEW(vi->view), &iter);
	g_free(text);
}

static void write(struct vi *vi)
{
	GtkTextIter start, end;
	char *text;
	GtkTextBuffer *buffer;
	if (!*vi->filename)
		return;
	buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(vi->view));
	gtk_text_buffer_get_start_iter(buffer, &start);
	gtk_text_buffer_get_end_iter(buffer, &end);
	text = gtk_text_buffer_get_text(buffer, &start, &end, TRUE);
	if (!g_file_set_contents(vi->filename, text, strlen(text), NULL))
		fprintf(stderr, "could not write file\n");
	g_free(text);
}

static void quit()
{
	gtk_main_quit();
}

static int startswith(char *heystack, char *needle)
{
	while (*needle && *needle == *heystack++)
		needle++;
	return !*needle;
}

void execcmd(struct vi *vi, char *cmd)
{
	if (!strcmp(":q", cmd) || !strcmp(":q!", cmd))
		quit();
	if (startswith(cmd, ":e "))
		edit(vi, cmd + 3);
	if (!strcmp(":w", cmd))
		write(vi);
	if (!strcmp(":wq", cmd)) {
		write(vi);
		quit();
	}
	if (startswith(cmd, "/") || startswith(cmd, "?")) {
		char *keyword = cmd + 1;
		strncpy(vi->word, keyword, MAX_COMMAND_LENGTH);
		vi->word[MAX_COMMAND_LENGTH - 1] = '\0';
	}
}
